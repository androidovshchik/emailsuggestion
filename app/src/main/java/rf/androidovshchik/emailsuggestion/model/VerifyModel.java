package rf.androidovshchik.emailsuggestion.model;

import com.google.gson.annotations.SerializedName;

public class VerifyModel {

    @SerializedName("result")
    public String result;

    @SerializedName("reason")
    public String reason;

    @SerializedName("disposable")
    public boolean disposable;

    @SerializedName("accept_all")
    public boolean acceptAll;

    @SerializedName("did_you_mean")
    public String didYouMean;

    @SerializedName("email")
    public String email;

    @SerializedName("user")
    public String user;

    @SerializedName("domain")
    public String domain;

    @SerializedName("success")
    public boolean success;

    @Override
    public String toString() {
        return "VerifyModel{" +
            "result='" + result + '\'' +
            ", reason='" + reason + '\'' +
            ", disposable=" + disposable +
            ", acceptAll=" + acceptAll +
            ", didYouMean='" + didYouMean + '\'' +
            ", email='" + email + '\'' +
            ", user='" + user + '\'' +
            ", domain='" + domain + '\'' +
            ", success=" + success +
            '}';
    }
}