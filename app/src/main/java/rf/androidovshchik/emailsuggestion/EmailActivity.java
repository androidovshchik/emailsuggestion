package rf.androidovshchik.emailsuggestion;

import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import rf.androidovshchik.emailsuggestion.model.VerifyModel;
import rf.androidovshchik.emailsuggestion.presenter.EmailPresenter;
import rf.androidovshchik.emailsuggestion.view.EmailView;
import timber.log.Timber;

public class EmailActivity extends AppCompatActivity implements EmailView {

    @BindView(R.id.emailWrapper)
    TextInputLayout emailWrapper;
    @BindView(R.id.email)
    AutoCompleteTextView email;
    @BindView(R.id.response)
    TextView response;

    private EmailAdapter adapter;

    private EmailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        ButterKnife.bind(this);
        presenter = new EmailPresenter();
        presenter.attachView(this);
        adapter = new EmailAdapter(getApplicationContext());
        email.setThreshold(1);
        email.setAdapter(adapter);
        email.setOnItemClickListener((AdapterView<?> adapterView, View view, int position, long id) -> {
            email.setText((String) adapterView.getItemAtPosition(position));
            email.setSelection(email.getText().length());
        });
    }

    @OnTextChanged(value = R.id.email, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterEmailInput(Editable editable) {
        String email = editable.toString();
        Timber.d("afterEmailInput: %s", email);
        if (TextUtils.isEmpty(email)) {
            emailWrapper.setErrorEnabled(true);
            emailWrapper.setError("You need to enter an email");
        } else {
            emailWrapper.setErrorEnabled(false);
            emailWrapper.setError(null);
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                adapter.clear();
                presenter.requestSuggestion(email.toLowerCase());
            }
        }
    }

    @UiThread
    @Override
    public void onRemoteSuggestion(VerifyModel model) {
        Timber.d(model.toString());
        response.setText(model.toString());
        if (model.didYouMean != null) {
            adapter.add(model.didYouMean);
            adapter.notifyDataSetChanged();
            email.showDropDown();
        }
    }

    @UiThread
    @Override
    public void onLogicSuggestion(ArrayList<String> choice) {
        if (choice.size() > 0) {
            adapter.addAll(choice);
            adapter.notifyDataSetChanged();
            email.showDropDown();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
