package rf.androidovshchik.emailsuggestion.remote;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rf.androidovshchik.emailsuggestion.model.VerifyModel;

public interface KickBoxAPI {

    String URL = "https://api.kickbox.com/v2/";

    String API_KEY = "test_49dc5cb9e6cf3065c3c9c7a35c8645cfe1ff6823a33d0d95e363eb7a11909a0a";

    @Headers("Accept: application/json")
    @GET("verify?apikey=" + API_KEY)
    Observable<VerifyModel> verifyEmail(@Query("email") String email);
}