package rf.androidovshchik.emailsuggestion;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;

public class EmailAdapter extends ArrayAdapter<String> {

    private Filter filter = new EmailFilter();

    public EmailAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1, new ArrayList<>());
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private class EmailFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence text) {
            FilterResults result = new FilterResults();
            ArrayList<String> items = new ArrayList<>();
            for(int i = 0 ; i < getCount(); i++) {
                items.add(getItem(i));
            }
            result.values = items;
            result.count = getCount();
            return result;
        }

        @Override
        protected void publishResults(CharSequence text, FilterResults results) {
            notifyDataSetChanged();
        }
    }
}