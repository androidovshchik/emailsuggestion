package rf.androidovshchik.emailsuggestion.view;

import java.util.ArrayList;

import rf.androidovshchik.emailsuggestion.model.VerifyModel;

public interface EmailView extends BaseView {

    void onRemoteSuggestion(VerifyModel model);

    void onLogicSuggestion(ArrayList<String> choice);
}
