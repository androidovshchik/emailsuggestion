package rf.androidovshchik.emailsuggestion.presenter;

import rf.androidovshchik.emailsuggestion.view.BaseView;

public interface Presenter<V extends BaseView> {

    void attachView(V view);

    void detachView();
}
