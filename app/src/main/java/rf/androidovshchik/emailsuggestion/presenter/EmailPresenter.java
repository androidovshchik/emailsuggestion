package rf.androidovshchik.emailsuggestion.presenter;

import android.text.TextUtils;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rf.androidovshchik.emailsuggestion.BuildConfig;
import rf.androidovshchik.emailsuggestion.model.VerifyModel;
import rf.androidovshchik.emailsuggestion.remote.KickBoxAPI;
import rf.androidovshchik.emailsuggestion.view.EmailView;
import timber.log.Timber;

public class EmailPresenter extends BasePresenter<EmailView> {

    private static final List<String> POPULAR_DOMAINS = Arrays.asList(
        "mail.ru",
        "gmail.com",
        "yandex.ru",
        "example.ru",
        "yahoo.com"
    );

    private KickBoxAPI api;

    public EmailPresenter() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(KickBoxAPI.URL)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                .setLenient()
                .create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build();
        api = retrofit.create(KickBoxAPI.class);
    }

    public void requestSuggestion(String email) {
        if (!TextUtils.isEmpty(email)) {
            ArrayList<String> choice = new ArrayList<>();
            // WARNING below logic may be harmful for mind
            if (!email.contains("@")) {
                for (String domain : POPULAR_DOMAINS) {
                    choice.add(email + "@" + domain);
                }
            } else if (email.endsWith("@")) {
                for (String domain : POPULAR_DOMAINS) {
                    choice.add(email + domain);
                }
            } else {
                // e.g. user@example.ru
                String[] inputParts = email.split("@");
                String inputUser = inputParts[0];
                String inputDomain = inputParts[1];
                for (String domain : POPULAR_DOMAINS) {
                    boolean matches = false;
                    int length = Math.min(inputDomain.length(), domain.length());
                    for (int c = 0; c < length; c++) {
                        if (inputDomain.charAt(c) == domain.charAt(c)) {
                            matches = true;
                            break;
                        }
                    }
                    if (matches) {
                        Timber.d("matches: %s", inputUser + "@" + domain);
                        choice.add(inputUser + "@" + domain);
                    }
                }
            }
            getView().onLogicSuggestion(choice);
            disposable.clear();
            disposable.add(api.verifyEmail(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse));
        }
    }

    private void handleResponse(VerifyModel model) {
        if (isViewAttached() && model != null) {
            getView().onRemoteSuggestion(model);
        }
    }
}
