package rf.androidovshchik.emailsuggestion.presenter;

import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.emailsuggestion.view.BaseView;

public class BasePresenter<T extends BaseView> implements Presenter<T> {

    protected CompositeDisposable disposable;

    private T view;

    @Override
    public void attachView(T view) {
        this.view = view;
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            disposable = null;
        }
        disposable = new CompositeDisposable();
    }

    @Override
    public void detachView() {
        disposable.dispose();
        disposable = null;
        view = null;
    }

    @Nullable
    public T getView() {
        return view;
    }

    @SuppressWarnings("unused")
    public boolean isViewAttached() {
        return view != null;
    }
}

