package rf.androidovshchik.emailsuggestion;

import android.app.Application;

import timber.log.Timber;

public class AppMain extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}
